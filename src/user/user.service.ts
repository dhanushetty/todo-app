import {
  BadRequestException,
  Injectable,
  NotFoundException,
} from '@nestjs/common';
import * as fs from 'fs';

@Injectable()
export class UserService {
  getUsers(): { id: number; name: string; blacklist: boolean }[] {
    return this.fetchData();
  }

  getSpecificUser(userId): {
    id: number;
    name: string;
    blacklist: boolean;
  } {
    const data = this.fetchData().find((ele) => ele.id === parseInt(userId));
    if (!data) {
      throw new NotFoundException('User not found.');
    }
    return data;
  }

  addUser(name, blacklist): { msg: string } {
    const data = this.fetchData();
    if (data.find((ele) => ele.name === name)) {
      throw new BadRequestException('User name already Exists.');
    }

    data.push({ id: data.length + 1, name, blacklist });

    fs.writeFileSync('./userdata.json', JSON.stringify(data));
    return { msg: 'New User added' };
  }

  updateUser(userId, name, blacklist): { msg: string } {
    const data = this.fetchData();
    if (!data.find((ele) => ele.id === parseInt(userId))) {
      throw new NotFoundException('User not found.');
    }
    if (
      name &&
      data.find(
        (ele) => ele.name.toLocaleLowerCase() === name.toLocaleLowerCase(),
      )
    ) {
      throw new BadRequestException('User name already Exists.');
    }
    const newData = data.map((ele) => {
      if (ele.id === parseInt(userId)) {
        console.log(name, blacklist);
        return {
          ...(name ? { name } : { name: ele.name }),
          blacklist,
          id: ele.id,
        };
      }
      return ele;
    });

    fs.writeFileSync('./userdata.json', JSON.stringify(newData));
    return { msg: ' User data updated.' };
  }

  deleteUser(userId): { msg: string } {
    const data = this.fetchData();
    if (!data.find((ele) => ele.id === parseInt(userId))) {
      throw new NotFoundException('User not found.');
    }

    const index = data.findIndex((ele) => ele.id === parseInt(userId));
    data.splice(index, 1);
    fs.writeFileSync('./userdata.json', JSON.stringify(data));

    return { msg: ' User data Deleted.' };
  }

  private fetchData(): { id: number; name: string; blacklist: boolean }[] {
    return JSON.parse(fs.readFileSync('./userdata.json', 'utf8'));
  }
}
