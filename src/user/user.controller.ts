import {
  Body,
  Controller,
  Delete,
  Get,
  Param,
  Patch,
  Post,
} from '@nestjs/common';
import { UserService } from './user.service';

@Controller('users')
export class UserController {
  constructor(private readonly userService: UserService) {}

  @Get()
  getUsers(): { id: number; name: string; blacklist: boolean }[] {
    return this.userService.getUsers();
  }

  @Get(':id')
  getSpeificUser(@Param('id') userId: number): {
    id: number;
    name: string;
    blacklist: boolean;
  } {
    return this.userService.getSpecificUser(userId);
  }

  @Post()
  addUser(@Body() userData: { name: string; blacklist: boolean }): {
    msg: string;
  } {
    return this.userService.addUser(userData.name, userData.blacklist);
  }

  @Patch(':id')
  updateUser(
    @Body() userData: { name: string; blacklist: boolean },
    @Param('id') userId: number,
  ): {
    msg: string;
  } {
    return this.userService.updateUser(
      userId,
      userData.name,
      userData.blacklist,
    );
  }

  @Delete(':id')
  deleteUser(@Param('id') userId: number): {
    msg: string;
  } {
    return this.userService.deleteUser(userId);
  }
}
