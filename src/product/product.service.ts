import {
  BadRequestException,
  Injectable,
  NotAcceptableException,
  NotFoundException,
} from '@nestjs/common';
import * as fs from 'fs';

@Injectable()
export class ProductService {
  getProducts(): { id: number; name: string; price: number }[] {
    return this.fetchData();
  }

  getSpecificProduct(prodId): { id: number; name: string; price: number } {
    const productData = this.fetchData().find(
      (ele) => ele.id === parseInt(prodId),
    );
    if (!productData) {
      throw new NotFoundException('Product not found.');
    }
    return productData;
  }

  addProduct(name, price): { msg: string } {
    const data = this.fetchData();
    if (data.find((ele) => ele.name === name)) {
      throw new BadRequestException('Product name already exists.');
    }
    data.push({ id: data.length + 1, name, price });
    fs.writeFileSync('./productData.json', JSON.stringify(data));
    return { msg: 'Successfully added a new product.' };
  }

  deleteSpecificProduct(prodId): { msg: string } {
    const data = this.fetchData();
    const index = data.findIndex((ele) => ele.id === parseInt(prodId));
    if (!index) {
      throw new NotAcceptableException('Product Not Found.');
    }
    data.splice(index, 1);
    fs.writeFileSync('./productData.json', JSON.stringify(data));
    return { msg: 'Successfully deletd a new product.' };
  }

  updateProdcut(prodId, name, price): { msg: string } {
    const data = this.fetchData();
    const newData = data.map((ele) => {
      if (ele.id === parseInt(prodId)) {
        return {
          ...(name ? { name } : { name: ele.name }),
          ...(price ? { price } : { price: ele.price }),
          id: ele.id,
        };
      }
      return ele;
    });
    fs.writeFileSync('./productData.json', JSON.stringify(newData));
    return { msg: 'Successfully updated a new product.' };
  }

  private fetchData(): { id: number; name: string; price: number }[] {
    return JSON.parse(fs.readFileSync('./productData.json', 'utf8'));
  }
}
