import {
  Body,
  Controller,
  Delete,
  Get,
  Param,
  Patch,
  Post,
} from '@nestjs/common';
import { ProductService } from './product.service';

@Controller('product')
export class ProductController {
  constructor(private readonly productService: ProductService) {}

  @Get()
  getProducts(): { id: number; name: string; price: number }[] {
    return this.productService.getProducts();
  }

  @Get(':id')
  getSpecificProduct(@Param('id') prodId: string): {
    id: number;
    name: string;
    price: number;
  } {
    return this.productService.getSpecificProduct(prodId);
  }

  @Post()
  addProduct(@Body() prodData: { name: string; price: number }): {
    msg: string;
  } {
    return this.productService.addProduct(prodData.name, prodData.price);
  }

  @Delete(':id')
  deleteProduct(@Param('id') prodId: string): {
    msg: string;
  } {
    return this.productService.deleteSpecificProduct(prodId);
  }

  @Patch(':id')
  updateProduct(
    @Body() prodData: { name: string; price: number },
    @Param('id') prodId: string,
  ): {
    msg: string;
  } {
    return this.productService.updateProdcut(
      prodId,
      prodData.name,
      prodData.price,
    );
  }
}
