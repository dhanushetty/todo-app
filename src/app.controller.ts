import { Controller, Get } from '@nestjs/common';
import { AppService } from './app.service';

@Controller('hello/new')
export class AppController {
  constructor(private readonly appService: AppService) {}

  @Get('world')
  getHello(): { a: number[] } {
    return this.appService.getHello();
  }
}
