import { Injectable } from '@nestjs/common';

@Injectable()
export class AppService {
  getHello(): { a: number[] } {
    const a = [];
    for (let i = 0; i <= 10; i++) {
      a.push(i);
    }
    return { a };
  }
}
