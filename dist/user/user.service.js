"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.UserService = void 0;
const common_1 = require("@nestjs/common");
const fs = require("fs");
let UserService = class UserService {
    getUsers() {
        return this.fetchData();
    }
    getSpecificUser(userId) {
        const data = this.fetchData().find((ele) => ele.id === parseInt(userId));
        if (!data) {
            throw new common_1.NotFoundException('User not found.');
        }
        return data;
    }
    addUser(name, blacklist) {
        const data = this.fetchData();
        if (data.find((ele) => ele.name === name)) {
            throw new common_1.BadRequestException('User name already Exists.');
        }
        data.push({ id: data.length + 1, name, blacklist });
        fs.writeFileSync('./userdata.json', JSON.stringify(data));
        return { msg: 'New User added' };
    }
    updateUser(userId, name, blacklist) {
        const data = this.fetchData();
        if (!data.find((ele) => ele.id === parseInt(userId))) {
            throw new common_1.NotFoundException('User not found.');
        }
        if (name &&
            data.find((ele) => ele.name.toLocaleLowerCase() === name.toLocaleLowerCase())) {
            throw new common_1.BadRequestException('User name already Exists.');
        }
        const newData = data.map((ele) => {
            if (ele.id === parseInt(userId)) {
                console.log(name, blacklist);
                return Object.assign(Object.assign({}, (name ? { name } : { name: ele.name })), { blacklist, id: ele.id });
            }
            return ele;
        });
        fs.writeFileSync('./userdata.json', JSON.stringify(newData));
        return { msg: ' User data updated.' };
    }
    deleteUser(userId) {
        const data = this.fetchData();
        if (!data.find((ele) => ele.id === parseInt(userId))) {
            throw new common_1.NotFoundException('User not found.');
        }
        const index = data.findIndex((ele) => ele.id === parseInt(userId));
        data.splice(index, 1);
        fs.writeFileSync('./userdata.json', JSON.stringify(data));
        return { msg: ' User data Deleted.' };
    }
    fetchData() {
        return JSON.parse(fs.readFileSync('./userdata.json', 'utf8'));
    }
};
UserService = __decorate([
    (0, common_1.Injectable)()
], UserService);
exports.UserService = UserService;
//# sourceMappingURL=user.service.js.map