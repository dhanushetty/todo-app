import { UserService } from './user.service';
export declare class UserController {
    private readonly userService;
    constructor(userService: UserService);
    getUsers(): {
        id: number;
        name: string;
        blacklist: boolean;
    }[];
    getSpeificUser(userId: number): {
        id: number;
        name: string;
        blacklist: boolean;
    };
    addUser(userData: {
        name: string;
        blacklist: boolean;
    }): {
        msg: string;
    };
    updateUser(userData: {
        name: string;
        blacklist: boolean;
    }, userId: number): {
        msg: string;
    };
    deleteUser(userId: number): {
        msg: string;
    };
}
