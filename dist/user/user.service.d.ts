export declare class UserService {
    getUsers(): {
        id: number;
        name: string;
        blacklist: boolean;
    }[];
    getSpecificUser(userId: any): {
        id: number;
        name: string;
        blacklist: boolean;
    };
    addUser(name: any, blacklist: any): {
        msg: string;
    };
    updateUser(userId: any, name: any, blacklist: any): {
        msg: string;
    };
    deleteUser(userId: any): {
        msg: string;
    };
    private fetchData;
}
