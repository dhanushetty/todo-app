import { ProductService } from './product.service';
export declare class ProductController {
    private readonly productService;
    constructor(productService: ProductService);
    getProducts(): {
        id: number;
        name: string;
        price: number;
    }[];
    getSpecificProduct(prodId: string): {
        id: number;
        name: string;
        price: number;
    };
    addProduct(prodData: {
        name: string;
        price: number;
    }): {
        msg: string;
    };
    deleteProduct(prodId: string): {
        msg: string;
    };
    updateProduct(prodData: {
        name: string;
        price: number;
    }, prodId: string): {
        msg: string;
    };
}
