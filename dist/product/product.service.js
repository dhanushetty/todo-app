"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.ProductService = void 0;
const common_1 = require("@nestjs/common");
const fs = require("fs");
let ProductService = class ProductService {
    getProducts() {
        return this.fetchData();
    }
    getSpecificProduct(prodId) {
        const productData = this.fetchData().find((ele) => ele.id === parseInt(prodId));
        if (!productData) {
            throw new common_1.NotFoundException('Product not found.');
        }
        return productData;
    }
    addProduct(name, price) {
        const data = this.fetchData();
        if (data.find((ele) => ele.name === name)) {
            throw new common_1.BadRequestException('Product name already exists.');
        }
        data.push({ id: data.length + 1, name, price });
        fs.writeFileSync('./productData.json', JSON.stringify(data));
        return { msg: 'Successfully added a new product.' };
    }
    deleteSpecificProduct(prodId) {
        const data = this.fetchData();
        const index = data.findIndex((ele) => ele.id === parseInt(prodId));
        if (!index) {
            throw new common_1.NotAcceptableException('Product Not Found.');
        }
        data.splice(index, 1);
        fs.writeFileSync('./productData.json', JSON.stringify(data));
        return { msg: 'Successfully deletd a new product.' };
    }
    updateProdcut(prodId, name, price) {
        const data = this.fetchData();
        const newData = data.map((ele) => {
            if (ele.id === parseInt(prodId)) {
                return Object.assign(Object.assign(Object.assign({}, (name ? { name } : { name: ele.name })), (price ? { price } : { price: ele.price })), { id: ele.id });
            }
            return ele;
        });
        fs.writeFileSync('./productData.json', JSON.stringify(newData));
        return { msg: 'Successfully updated a new product.' };
    }
    fetchData() {
        return JSON.parse(fs.readFileSync('./productData.json', 'utf8'));
    }
};
ProductService = __decorate([
    (0, common_1.Injectable)()
], ProductService);
exports.ProductService = ProductService;
//# sourceMappingURL=product.service.js.map