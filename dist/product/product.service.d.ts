export declare class ProductService {
    getProducts(): {
        id: number;
        name: string;
        price: number;
    }[];
    getSpecificProduct(prodId: any): {
        id: number;
        name: string;
        price: number;
    };
    addProduct(name: any, price: any): {
        msg: string;
    };
    deleteSpecificProduct(prodId: any): {
        msg: string;
    };
    updateProdcut(prodId: any, name: any, price: any): {
        msg: string;
    };
    private fetchData;
}
